# eicsim

Dihadron analysis for EIC simulations

# Quickstart
- create a Docker container with `bin/runEscalate.sh`
  - this container will be auto-removed upon exit; to disable auto-remove,
    pass a single argument, e.g. `bin/runEscalate.sh 1`
  - if auto-removal is disabled, the container will be named `eic`
    - use `bin/startEIC.sh` to start it again
    - use `bin/shellEIC.sh` to spawn a new bash shell
- inside the container, `cd dihadronAnalysis`
  - to run `ejana` with the `dihadron` plugin, execute `python3 runEjana.py`
    - running without any arguments will print a help page, explaining
      usage
    - example for reading a pythia file, processing 1000 events:
      - `python3 runEjana.py -t pythia -i thePythiaFile.txt -n 1000`
    - example for reading a pythia file, processing all the events:
      - `python3 runEjana.py -t pythia -i thePythiaFile.txt -n 0`
    - example for how to read a hepMC file, and enable `eic_smear` 
      with `handbook`
      configuration:
      - `python3 runEjana.py -t hepmc -i theHepMCfile.hepmc -n 0 -s handbook`
    - note that is also important to specify the beam energies, e.g. `-e 10 -p 100`
