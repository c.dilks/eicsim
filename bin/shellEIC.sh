#!/bin/bash
# open a new bash window for a running "eic" container
eichome=/home/eicuser
docker exec -it eic bash \
-c "export PATH=${PATH}:${eichome}/bin:${eichome}/binloc && exec bash"
