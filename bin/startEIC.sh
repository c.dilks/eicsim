#!/bin/bash
# start and attach the eic container
# (use `docker ps -a` to see if it's running;
#  if not, you must "run" it first, using `docker run ...`)

docker start eic
docker attach eic

# alternative:
#conthome=/home/eicuser # container home directory
#docker start eic
#docker exec -it eic /bin/bash \
  #-c "export PATH=${PATH}:${conthome}/bin:${conthome}/binloc && exec bash"
#docker stop eic
