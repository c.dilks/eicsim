#!/bin/bash
# runs `docker run` on `$image`
# - pass an argument to create a container named `eic` which will not be auto-removed


# if argument was supplied, name the container with the argument, and disable auto-remove
args="-it"
if [ $# -gt 0 ]; then 
  args="$args --name $1"
else 
  args="$args --rm"
fi


# publishing / interaction
args="$args -v /tmp/.X11-unix:/tmp/.X11-unix:ro -e DISPLAY" # enable X11
#args="$args -p 8888:8888" # enable jupyter
#args="$args -p 6080:6080" # anable novnc


# bindmount eicsim
eicsim=$(pwd)
conthome=/home/eicuser # container home directory
args=$args\
"""
-v ${eicsim}/bin:${conthome}/bin:ro \
-v ${eicsim}/dihadronAnalysis:${conthome}/workspace/dihadronAnalysis:rw \
"""
expPath="${PATH}:${conthome}/bin"


# if ~/.loadDotfiles exists, load personal dot files
if [ -f ${HOME}/.loadDotfiles ]; then
  hosthome=/home/$(whoami | sed 's/docker$//g')
  args=$args\
"""
  -v ${hosthome}/e/share:${conthome}/workspace/share:rw \
  -v ${hosthome}/bin:${conthome}/binloc:ro \
  -v ${hosthome}/.vim:${conthome}/.vim:ro -v ${hosthome}/.vimrc:${conthome}/.vimrc:ro \
  -v ${hosthome}/.dir_colors:${conthome}/.dir_colors:ro -e TERM=xterm-256color \
  -v ${hosthome}/.Xdefaults:${conthome}/.Xdefaults:ro \
"""
  expPath="${expPath}:${conthome}/binloc"
  extraRC=\
"""
export PS1='\\[\\e[1;33m\\][\\u@\\h \\W] >>> \\[\\e[m\\]'
eval \`dircolors -b ${conthome}/.dir_colors\`
"""
else
  extraRC=""
fi


# append to bashrc
cat > ${eicsim}/.extrarc << EOF

# CUSTOM SETTTINGS FROM eicsim REPOSITORY"
export PATH=\${PATH}:$expPath
$extraRC
alias grep='grep --color'
alias ls='ls -p --color'
alias ll='ls -lhp --color'
alias cp='cp -v'
alias mv='mv -v'
alias rm='rm -v'
alias rootq='root -b -q'
alias ..='cd ..'
EOF
args="${args} -v ${eicsim}/.extrarc:${conthome}/.extrarc:ro"


# run container (latest)
docker run $args \
  electronioncollider/escalate:latest bash \
  -c "cat ${conthome}/.extrarc >> ${conthome}/.bashrc && exec bash"

# run stable version
#echo "Note: running escalate image 1.0.0, but updated versions exist"
#docker run $args \
  #electronioncollider/escalate:1.0.0 bash \
  #-c "cat ${conthome}/.extrarc >> ${conthome}/.bashrc && exec bash"
