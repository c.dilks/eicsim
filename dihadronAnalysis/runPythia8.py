# test pythia8, without using dire

import sys
sys.path.insert(0,"/home/eicuser/app/pythia8/pythia8-pythia8244/lib")

import pythia8
pythia = pythia8.Pythia()
pythia.readFile("steer/pythia8withoutDire.cmnd")
pythia.init()

nEvents = pythia.mode("Main:numberOfEvents")
#print(nEvents)
for i in range(0,nEvents):
    print(i)
    if not pythia.next(): continue
pythia.stat()
