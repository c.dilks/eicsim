// search for CUT (or CUTs), which identify all cuts used

#include "DihadronProcessor.h"

using namespace std;
using namespace fmt;
using namespace minimodel;
using namespace ej;
using namespace dih;


void DihadronProcessor::Init() {

  Tools::PrintTitleBox("DihadronProcessor::Init()");
  errThrown = false;

  // initialize class that holds histograms and other root objects
  // 'services' is a service locator, we ask it for TFile
  rootOutput.init(services.Get<TFile>());


  // set parameters
  auto pm = services.Get<JParameterManager>();
  verbose = 0;
  pm->SetDefaultParameter("dihadron:verbose",verbose,"verbose output");
  pm->SetDefaultParameter("dihadron:fileType",fileType,"source file type");
  pm->SetDefaultParameter("dihadron:EbeamEn",EbeamEn,"electron beam energy");
  pm->SetDefaultParameter("dihadron:PbeamEn",PbeamEn,"proton beam energy");
  print("Parameters:\n");
  print("  dihadron:verbose: {}\n",verbose);
  print("  dihadron:EbeamEn: {}\n",EbeamEn);
  print("  dihadron:PbeamEn: {}\n",PbeamEn);
  print("  dihadron:fileType: {}\n",fileType);



  // set electron z-direction sign (use -1 to match handbook)
  // - flexibility here in case you are reading a file which has the
  //   opposite convention
  if(fileType=="hepmc") electronSign = -1;
  else if(fileType=="pythia") electronSign = -1;
  else if(fileType=="lund") electronSign = -1;
  else {
    // TODO: improve error handling; fprintf doesn't print inline,
    // would `jerr` (and `jout`) work?
    print("ERROR: DihadronProcessor: invalid fileType\n");
    errThrown = true;
  };

/////////////////////////////////
  /*
  // extra branches for compatibility with CLAS dihadron asymmetry code
  rootOutput.tree->Branch("runnum",&runnum);
  rootOutput.tree->Branch("particleCnt",&particleCnt);
  rootOutput.tree->Branch("eleStatus",&eleStatus);
  rootOutput.tree->Branch("eleChi2pid",&eleChi2pid);
  rootOutput.tree->Branch("eleFidPCAL",eleFidPCAL,"eleFidPCAL[3]/O");
  rootOutput.tree->Branch("eleFidDC",eleFidDC,"eleFidDC[3]/O");
  rootOutput.tree->Branch("hadStatus",hadStatus,"hadStatus[2]/I");
  rootOutput.tree->Branch("hadChi2pid",hadChi2pid,"hadChi2pid[2]/F");
  */
/////////////////////////////////



  /////////////////////////
  //
  //   Smeared tree branches

  //  rootOutput.tree_s->Branch("spinE",&spinE); // incident electron spin
  //  rootOutput.tree_s->Branch("spinP",&spinP); // incident proton spin

  // DIS kinematics
  rootOutput.tree_s->Branch("Q2",&Q2);
  rootOutput.tree_s->Branch("x",&x);
  rootOutput.tree_s->Branch("y",&y);
  rootOutput.tree_s->Branch("W",&W);
  //  rootOutput.tree_s->Branch("Nu",&Nu);
  if(fileType=="pythia") {
    // if reading pythia, event records include "true" DIS kinematics
    rootOutput.tree_s->Branch("Q2_pythia",&Q2_pythia);
    rootOutput.tree_s->Branch("x_pythia",&x_pythia);
    rootOutput.tree_s->Branch("y_pythia",&y_pythia);
    rootOutput.tree_s->Branch("W_pythia",&W_pythia);
    //    rootOutput.tree_s->Branch("Nu_pythia",&Nu_pythia);
  };

  //    // Smeared photon pairing & kinematics
  //  rootOutput.tree_s->Branch("photIdx",photIdx,"photIdx[2]/I");
  //  rootOutput.tree_s->Branch("photMotherPdg",photMotherPdg,"photMotherPdg[h]/I");
  //  rootOutput.tree_s->Branch("photMotherI",photMotherI,"photMotherI[h]/I");
  //  rootOutput.tree_s->Branch("photI",hadI,"hadI[2]/F"); // particle number from
  //  rootOutput.tree_s->Branch("smearMatch",smearMatch,"smearMatch[2]/I");
  //  rootOutput.tree_s->Branch("photE",photE,"photE[2]/F");
  //  rootOutput.tree_s->Branch("photP",photP,"photP[2]/F");
  //  rootOutput.tree_s->Branch("photEta",photEta,"photEta[2]/F");


  //  // scattered electron kinematics
  //  rootOutput.tree_s->Branch("eleI",&eleI); // particle record number
  //  rootOutput.tree_s->Branch("eleE",&eleE); // energy
  //  rootOutput.tree_s->Branch("eleP",&eleP); // momentum
  rootOutput.tree_s->Branch("eleSmearE",&eleSmearE,"eleSmearE/O");
  rootOutput.tree_s->Branch("eleSmearP",&eleSmearP,"eleSmearP/O");
  rootOutput.tree_s->Branch("eleSmearPID",&eleSmearPID,"eleSmearPID/O");
  rootOutput.tree_s->Branch("eleSmearVtx",&eleSmearVtx,"eleSmearVtx/O");


  // dihadron pairing
  // - hadIdx is a single-digit hexadecimal number representing a hadron,
  //   e.g., 0x3 represents pi+
  // - pairType is a 2-digit hexadecimal number with each digit a hadron
  // - run the script `../PrintEnumerators.C` for details
  // - `pairType==0x34` is for pi+ pi- dihadrons


  //dihadrons
  // particle record; can be used with evtNr for indexing TTree friends
  rootOutput.tree_s->Branch("numHadronPairs",&numHadronPairs); // iterator for dihadrons
  //  rootOutput.tree_s->Branch("hadIdx",hadIdx,"hadIdx[2]/I");
  //    rootOutput.tree_s->Branch("hadPDG",hadPDG,"hadPDG[numHadronPairs][2]/I");
  rootOutput.tree_s->Branch("hadI",hadI,"hadI[numHadronPairs][2]/I"); // particle number from
  //  rootOutput.tree_s->Branch("hadE",hadE,"hadE[numHadronPairs][2]/F");
  rootOutput.tree_s->Branch("hadPperp",hadPperp,"hadPperp[numHadronPairs][2]/F");
  //  rootOutput.tree_s->Branch("hadP",hadP,"hadP[numHadronPairs][2]/F");
  //  rootOutput.tree_s->Branch("hadM",hadM,"hadM[numHadronPairs][2]/F");
  //  rootOutput.tree_s->Branch("dir_x",hadDirX,"hadDirX[numHadronPairs][2]/F");
  //  rootOutput.tree_s->Branch("dir_y",hadDirY,"hadDirY[numHadronPairs][2]/F");
  //  rootOutput.tree_s->Branch("dir_z",hadDirZ,"hadDirZ[numHadronPairs][2]/F");
  rootOutput.tree_s->Branch("hadSmearE",hadSmearE,"hadSmearE[numHadronPairs][2]/O");
  rootOutput.tree_s->Branch("hadSmearP",hadSmearP,"hadSmearP[numHadronPairs][2]/O");
  rootOutput.tree_s->Branch("hadSmearPID",hadSmearPID,"hadSmearPID[numHadronPairs][2]/O");
  rootOutput.tree_s->Branch("hadSmearVtx",hadSmearVtx,"hadSmearVtx[numHadronPairs][2]/O");


  // dihadron kinematics
  rootOutput.tree_s->Branch("Mx",Mx, "Mx[numHadronPairs]/F"); // missing mass
  rootOutput.tree_s->Branch("evtNr",&evtNr); // event number
  rootOutput.tree_s->Branch("polarization",&spinP); // polarization random +/-1
  rootOutput.tree_s->Branch("phiR",phiR, "phiR[numHadronPairs]/F"); // angle between RT and scatter plane
  rootOutput.tree_s->Branch("phiH",phiH, "phiH[numHadronPairs]/F"); // angle between Ph and scatter plane
  rootOutput.tree_s->Branch("phiS",phiS, "phiS[numHadronPairs]/F"); // angle between S and scatter plane
  rootOutput.tree_s->Branch("z",z, "z[numHadronPairs]/F"); // z of the dihadron
  rootOutput.tree_s->Branch("M",M, "M[numHadronPairs]/F"); // invariant mass
  rootOutput.tree_s->Branch("theta",theta, "theta[numHadronPairs]/F"); // dihadron CoM decay angle
  rootOutput.tree_s->Branch("xF",xF, "xF[numHadronPairs]/F"); // Feynman-x of the dihadron
  rootOutput.tree_s->Branch("pT",PhPerpMag, "PhPerpMag[numHadronPairs]/F"); // transverse component of dihadron total momentum (perp frame)
  rootOutput.tree_s->Branch("pairType",pairType, "pairType[numHadronPairs]/I");
  rootOutput.tree_s->Branch("pairMatch",pairMatch,"pairMatch[numHadronPairs]/I");


  /////////////////////////
  //
  //   Not Smeared tree branches

  //    rootOutput.tree_ns->Branch("spinE",&spinE); // incident electron spin
  //    rootOutput.tree_ns->Branch("spinP",&spinP); // incident proton spin

  // DIS kinematics
  rootOutput.tree_ns->Branch("Q2",&Q2);
  rootOutput.tree_ns->Branch("x",&x);
  rootOutput.tree_ns->Branch("y",&y);
  rootOutput.tree_ns->Branch("W",&W);
  //    rootOutput.tree_ns->Branch("Nu",&Nu);
  if(fileType=="pythia") {
    // if reading pythia, event records include "true" DIS kinematics
    rootOutput.tree_ns->Branch("Q2_pythia",&Q2_pythia);
    rootOutput.tree_ns->Branch("x_pythia",&x_pythia);
    rootOutput.tree_ns->Branch("y_pythia",&y_pythia);
    rootOutput.tree_ns->Branch("W_pythia",&W_pythia);
    //      rootOutput.tree_ns->Branch("Nu_pythia",&Nu_pythia);
  };

  //    // Smeared photon pairing & kinematics
  //  rootOutput.tree_ns->Branch("photIdx",photIdx,"photIdx[2]/I");
  //  rootOutput.tree_ns->Branch("photMotherPdg",photMotherPdg,"photMotherPdg[h]/I");
  //  rootOutput.tree_ns->Branch("photMotherI",photMotherI,"photMotherI[h]/I");
  //  rootOutput.tree_ns->Branch("photI",hadI,"hadI[2]/F"); // particle number from
  //  rootOutput.tree_ns->Branch("smearMatch",smearMatch,"smearMatch[2]/I");
  //  rootOutput.tree_ns->Branch("photE",photE,"photE[2]/F");
  //  rootOutput.tree_ns->Branch("photP",photP,"photP[2]/F");
  //  rootOutput.tree_ns->Branch("photEta",photEta,"photEta[2]/F");
  //
  //    // scattered electron kinematics
  //    rootOutput.tree_ns->Branch("eleI",&eleI); // particle record number
  //    rootOutput.tree_ns->Branch("eleE",&eleE); // energy
  //    rootOutput.tree_ns->Branch("eleP",&eleP); // momentum
  rootOutput.tree_ns->Branch("eleSmearE",&eleSmearE,"eleSmearE/O");
  rootOutput.tree_ns->Branch("eleSmearP",&eleSmearP,"eleSmearP/O");
  rootOutput.tree_ns->Branch("eleSmearPID",&eleSmearPID,"eleSmearPID/O");
  rootOutput.tree_ns->Branch("eleSmearVtx",&eleSmearVtx,"eleSmearVtx/O");


  // dihadron pairing
  // - hadIdx is a single-digit hexadecimal number representing a hadron,
  //   e.g., 0x3 represents pi+
  // - pairType is a 2-digit hexadecimal number with each digit a hadron
  // - run the script `../PrintEnumerators.C` for details
  // - `pairType==0x34` is for pi+ pi- dihadrons


  // dihadrons
  // particle record; can be used with evtNr for indexing TTree friends
  rootOutput.tree_ns->Branch("numHadronPairs",&numHadronPairs); // iterator for dihadrons
  //    rootOutput.tree_ns->Branch("hadIdx",hadIdx,"hadIdx[numHadronPairs][2]/I");
  //    rootOutput.tree_ns->Branch("hadPDG",hadPDG,"hadPDG[numHadronPairs][2]/I");
  rootOutput.tree_ns->Branch("hadI",hadI,"hadI[numHadronPairs][2]/I"); // particle number from
  //    rootOutput.tree_ns->Branch("hadE",hadE,"hadE[numHadronPairs][2]/F");
  rootOutput.tree_ns->Branch("hadPperp",hadPperp,"hadPperp[numHadronPairs][2]/F");
  //    rootOutput.tree_ns->Branch("hadP",hadP,"hadP[numHadronPairs][2]/F");
  //    rootOutput.tree_ns->Branch("hadM",hadM,"hadM[numHadronPairs][2]/F");
  //    rootOutput.tree_ns->Branch("dir_x",hadDirX,"hadDirX[numHadronPairs][2]/F");
  //    rootOutput.tree_ns->Branch("dir_y",hadDirY,"hadDirY[numHadronPairs][2]/F");
  //    rootOutput.tree_ns->Branch("dir_z",hadDirZ,"hadDirZ[numHadronPairs][2]/F");
  rootOutput.tree_ns->Branch("hadSmearE",hadSmearE,"hadSmearE[numHadronPairs][2]/O");
  rootOutput.tree_ns->Branch("hadSmearP",hadSmearP,"hadSmearP[numHadronPairs][2]/O");
  rootOutput.tree_ns->Branch("hadSmearPID",hadSmearPID,"hadSmearPID[numHadronPairs][2]/O");
  rootOutput.tree_ns->Branch("hadSmearVtx",hadSmearVtx,"hadSmearVtx[numHadronPairs][2]/O");


  // dihadron kinematics
  rootOutput.tree_ns->Branch("Mx",Mx, "Mx[numHadronPairs]/F"); // missing mass
  rootOutput.tree_ns->Branch("evtNr",&evtNr); // event number
  rootOutput.tree_ns->Branch("polarization",&spinP); // polarization random +/-1
  rootOutput.tree_ns->Branch("phiR",phiR, "phiR[numHadronPairs]/F"); // angle between RT and scatter plane
  rootOutput.tree_ns->Branch("phiH",phiH, "phiH[numHadronPairs]/F"); // angle between Ph and scatter plane
  rootOutput.tree_ns->Branch("phiS",phiS, "phiS[numHadronPairs]/F"); // angle between S and scatter plane
  rootOutput.tree_ns->Branch("z",z, "z[numHadronPairs]/F"); // z of the dihadron
  rootOutput.tree_ns->Branch("M",M, "M[numHadronPairs]/F"); // invariant mass
  rootOutput.tree_ns->Branch("theta",theta, "theta[numHadronPairs]/F"); // dihadron CoM decay angle
  rootOutput.tree_ns->Branch("xF",xF, "xF[numHadronPairs]/F"); // Feynman-x of the dihadron
  rootOutput.tree_ns->Branch("pT",PhPerpMag, "PhPerpMag[numHadronPairs]/F"); // transverse component of dihadron total momentum (perp frame)
  rootOutput.tree_ns->Branch("pairType",pairType, "pairType[numHadronPairs]/I");
  rootOutput.tree_ns->Branch("pairMatch",pairMatch, "pairMatch[numHadronPairs]/I");

  rootOutput.tree_ns->Branch("weight",myAUTweight, "myAUTweight[numHadronPairs]/F");
  rootOutput.tree_ns->Branch("weightLowerLimit",weightLowerLimit,"weightLowerLimit[numHadronPairs]/F");
  rootOutput.tree_ns->Branch("weightUpperLimit",weightUpperLimit,"weightUpperLimit[numHadronPairs]/F");



  //
  /////////////////////////



  // set beam vectors
  EbeamP = Tools::EMtoP(EbeamEn,PartMass(kE));
  PbeamP = Tools::EMtoP(PbeamEn,PartMass(kP));
  vecEbeam.SetPxPyPzE( 0, 0, electronSign * EbeamP, EbeamEn );
  vecPbeam.SetPxPyPzE( PbeamP * TMath::Sin(crossingAngle), 0, -1*electronSign * PbeamP * TMath::Cos(crossingAngle), PbeamEn );

  evtNr_local = 0;

  RNG = new TRandomMixMax(1421);

  autTesting = new AUTweight();

  Tools::PrintTitleBox("DihadronProcessor::Init() complete");

}


void DihadronProcessor::Process(const std::shared_ptr<const JEvent> &event) {

  if(errThrown) return;

  // mutex lock:
  // - everything above is done in parallel
  //   (only allowed to use local variables and `event` here)
  // - everything below is done sequentially
  std::lock_guard<std::recursive_mutex> lock(rootOutput.lock);



  // get generated particles

  static bool said_once = false;

  std::vector<const minimodel::McGeneratedParticle*> gen_parts_ns;
  std::vector<const minimodel::McGeneratedParticle*> gen_parts_s;

  // SMEARING
  bool smeared_taken = event->GetFactory<McGeneratedParticle>("smear", false);
  if(smeared_taken){
    gen_parts_ns = event->Get<McGeneratedParticle>();
    gen_parts_s = event->Get<McGeneratedParticle>("smear");
  } else {
    gen_parts_ns = event->Get<McGeneratedParticle>();
  };

  if(!said_once) {
    said_once = true;
    print("JFactoryT<DihadronInputParticle> Smeared taken = {}\n", smeared_taken);
    Tools::PrintSeparator(50);
  }

  std::vector<DihadronInputParticle*> input_parts_ns;
  std::vector<DihadronInputParticle*> input_parts_s;
  std::vector< float > ns_idxList;
  std::vector< float > s_idxList;
  std::vector<DihadronInputParticle*> parList_ns_sorted;
  std::vector<DihadronInputParticle*> parList_s_sorted;
  std::vector<std::vector<DihadronInputParticle*>> parList_full;

  for (auto gen_part : gen_parts_ns) {

    // set variables
    auto input_part = new DihadronInputParticle();

    input_part->idx = gen_part->id;
    input_part->is_stable = gen_part->is_stable;
    input_part->pdg = gen_part->pdg;
    input_part->mother_idx = gen_part->mother_id;
    input_part->mother_second_idx = gen_part->mother_second_id;
    for(auto mother_part : gen_parts_ns) {
      if (mother_part->id == gen_part->mother_id) input_part->mother_pdg = mother_part->pdg;
      if (mother_part->id == gen_part->mother_second_id) input_part->mother_second_pdg = mother_part->pdg;
    }

    input_part->p = ROOT::Math::PxPyPzMVector(gen_part->px, gen_part->py, gen_part->pz, gen_part->m);
    input_part->E = ROOT::Math::PxPyPzEVector(gen_part->dir_x * gen_part->tot_e, gen_part->dir_y * gen_part->tot_e, gen_part->dir_z * gen_part->tot_e, gen_part->tot_e);
    input_part->vertex.SetXYZ(gen_part->vtx_x, gen_part->vtx_y, gen_part->vtx_z);

    input_part->smearE = gen_part->smear.has_e;
    input_part->smearP = gen_part->smear.has_p;
    input_part->smearPID = gen_part->smear.has_pid;
    input_part->smearVtx = gen_part->smear.has_vtx;

    input_parts_ns.push_back(input_part);
  }

  if(smeared_taken){

    for (auto gen_part : gen_parts_s){

      // set variables
      auto input_part = new DihadronInputParticle();

      input_part->idx = gen_part->id;
      input_part->is_stable = gen_part->is_stable;
      input_part->pdg = gen_part->pdg;
      input_part->mother_idx = gen_part->mother_id;
      for(auto mother_part : gen_parts_s) {
        if (mother_part->id == gen_part->mother_id) input_part->mother_pdg = mother_part->pdg;
        if (mother_part->id == gen_part->mother_second_id) input_part->mother_second_pdg = mother_part->pdg;
      }

      input_part->p = ROOT::Math::PxPyPzMVector(gen_part->px, gen_part->py, gen_part->pz, gen_part->m);
      input_part->E = ROOT::Math::PxPyPzEVector(gen_part->dir_x * gen_part->tot_e, gen_part->dir_y * gen_part->tot_e, gen_part->dir_z * gen_part->tot_e, gen_part->tot_e);
      input_part->vertex.SetXYZ(gen_part->vtx_x, gen_part->vtx_y, gen_part->vtx_z);

      input_part->smearE = gen_part->smear.has_e;
      input_part->smearP = gen_part->smear.has_p;
      input_part->smearPID = gen_part->smear.has_pid;
      input_part->smearVtx = gen_part->smear.has_vtx;

      input_parts_s.push_back(input_part);

    }

    // connection between smeared and not smeared - through index of original particle list

    for (auto par_ns : input_parts_ns){
      for (auto par_s : input_parts_s){
        if (par_ns->idx == par_s->idx){
          ns_idxList.push_back(par_ns->idx);
          s_idxList.push_back(par_s->idx);
          par_ns->smearMatch = 1;
          par_s->smearMatch = 1;
          parList_ns_sorted.push_back(par_ns);
          parList_s_sorted.push_back(par_s);
        }
      }
    }

    for (auto remainingPar : input_parts_ns){
      if (find(ns_idxList.begin(), ns_idxList.end(), remainingPar->idx) == ns_idxList.end()) {
        remainingPar->smearMatch = 0;
        parList_ns_sorted.push_back(remainingPar);
      }
    }

    for (auto remainingPar : input_parts_s){
      if (find(s_idxList.begin(), s_idxList.end(), remainingPar->idx) == s_idxList.end()) {
        remainingPar->smearMatch = 0;
        parList_s_sorted.push_back(remainingPar);
      }
    }

    parList_full.push_back(parList_ns_sorted);
    parList_full.push_back(parList_s_sorted);

  } //if smearing on loop

  else parList_full.push_back(input_parts_ns);


  //------------------------------------
  // Photon loop for pi0 identification
  //------------------------------------


  if (smeared_taken) {

    std::vector<DihadronInputParticle*> pion_list_tmp;
    int c = parList_full[0].size() + 1;

    for(auto photA : parList_full[1]) {
      if(photA->pdg == PartPID(kPhoton)){
        for (auto photB : parList_full[1]){
          if(photB->pdg == PartPID(kPhoton)){
            phot[qA] = photA;
            phot[qB] = photB;

            // check the particle "I": id in the list of this event's particles & get mother info
            for(h=0; h<2; h++){
              photIdx_tmp[h] = PIDtoIdx(phot[h]->pdg);
              photI_tmp[h] = phot[h]->idx;
              //                    photMotherI_tmp[h] = phot[h]->mother_idx;
              //                    photMotherPdg_tmp[h] = phot[h]->mother_pdg;
            }

            //                 check acceptance
            for(h=0; h<2; h++){
              if(phot[h]->smearE == 0 || phot[h]->smearP == 0) continue;
            }

            //                 avoid double-counting
            if(photI_tmp[qA]==photI_tmp[qB]) continue;
            if(photIdx_tmp[qA]==photIdx_tmp[qB] && photI_tmp[qA]>photI_tmp[qB]) continue;

            for(h=0; h<2; h++){
              photE_tmp[h] = phot[h]->E.E();
              vecPhot_tmp[h].SetPxPyPzE(phot[h]->E.px(), phot[h]->E.py(), phot[h]->E.pz(), photE_tmp[h]);
            }

            if (photE_tmp[qA] < .1 || photE_tmp[qB] < .1) continue;

            vecHad_tmp = vecPhot_tmp[qA] + vecPhot_tmp[qB];
            hadM_tmp = vecHad_tmp.M();

            if(.12 < hadM_tmp && hadM_tmp < .15){
              E_asym = (photE_tmp[qB] - photE_tmp[qA]) / (photE_tmp[qA] + photE_tmp[qB]);
              if (-0.8 < E_asym && E_asym < 0.8){
                auto input_part = new DihadronInputParticle();

                input_part->pdg = 111;
                input_part->E = ROOT::Math::PxPyPzEVector(vecHad_tmp.Px(), vecHad_tmp.Py(), vecHad_tmp.Pz(), vecHad_tmp.E());

                input_part->idx = c;
                c++;
                input_part->photA_idx = photI_tmp[qA];
                input_part->photB_idx = photI_tmp[qB];
                input_part->p = ROOT::Math::PxPyPzMVector(vecHad_tmp.Px(), vecHad_tmp.Py(), vecHad_tmp.Pz(), hadM_tmp);
                input_part->smearE = 1;
                input_part->smearP = 1;
                input_part->smearPID = 0;

                pion_list_tmp.push_back(input_part);

                for(h=0; h<2; h++){
                  photE[h] = photE_tmp[h]; // ->tree
                  photP[h] = vecPhot[h].P(); // ->tree
                  photEta[h] = vecPhot[h].Eta(); // ->tree

                } // Phot tree
              } // Energy restriction
            } // Mass restriction
          } // Phot B
        } // Particle B
      } // Phot A
    } // Particle A

    parList_full[1].insert( parList_full[1].end(), pion_list_tmp.begin(), pion_list_tmp.end() );
  }

  //--------------------------------------------------------------------
  // dihadron & electron identification in new particle list with pi0s
  //--------------------------------------------------------------------

  // first loop over parList_ns_sorted, then over parList_s_sorted
  for (int s=0; s<parList_full.size(); s++){

    // get event-level variables, such as evtNr and spin
    //evtNr = event->GetEventNumber(); // broken
    if(fileType=="lund") {
      auto lundEvent = event->Get<LundEventData>();
      evtNr = evtNr_local++; // ->tree // local event number // TODO
      particleCnt = lundEvent[0]->column_values[0]; // ->tree
      spinP = lundEvent[0]->column_values[3]; // ->tree
      spinE = lundEvent[0]->column_values[4]; // ->tree
    }
    else if(fileType=="pythia") {
      auto lundEvent = event->Get<LundEventData>();
      evtNr = lundEvent[0]->column_values[1]; // ->tree
      particleCnt = 0; // ignore // ->tree
      spinP = RNG->Integer(2)==0 ? -1 : 1; // 50/50 random spin // ->tree
      spinE = 0; // ->tree
      y_pythia = lundEvent[0]->column_values[10]; // ->tree
      Q2_pythia = lundEvent[0]->column_values[11]; // ->tree
      x_pythia = lundEvent[0]->column_values[12]; // ->tree
      W_pythia = TMath::Sqrt(lundEvent[0]->column_values[13]); // ->tree
      Nu_pythia = lundEvent[0]->column_values[14]; // ->tree
    } else if(fileType=="hepmc") {
      // TODO
      auto hepmcEvent = event->Get<HepMCEventData>();
      // see HepMC package GenEvent.h for accessors
      evtNr = hepmcEvent[0]->hepmc_event->event_number(); // ->tree
      particleCnt = 0; // ignore // ->tree
      spinP = RNG->Integer(2)==0 ? -1 : 1; // 50/50 random spin // ->tree
      spinE = 0; // ->tree
    };

    // event-level printout
    if(verbose>=1) this->PrintEvent();



    //---------------------
    // electron loop
    //---------------------

    eleFound = false;
    eleEmax = 0;
    bool isEle;
    for(auto par : parList_full[s]) {
      if(!(par->is_stable)) continue;

      // particle printout
      if(verbose>=1) this->PrintParticle(par);

      // check if PID is electron
      if(par->pdg == PartPID(kE) ) {

        vecEleTmp.SetXYZM(par->p.px(), par->p.py(), par->p.pz(), PartMass(kE));
        eleETmp = vecEleTmp.E();

        // CUT out electrons which have E > electron beam energy
        //if(eleETmp > EbeamEn) continue; // -> skip the event
        //if(eleETmp < EbeamEn) continue; // -> testing

        // check if this is the max-energy electron
        if( eleETmp > eleEmax ) {
          vecEle.SetXYZM(par->p.px(), par->p.py(), par->p.pz(), PartMass(kE));
          eleE = vecEle.E(); // ->tree
          eleI = par->idx; // ->tree
          eleVertex[eX] = par->vertex.x(); // ->tree
          eleVertex[eY] = par->vertex.y(); // ->tree
          eleVertex[eZ] = par->vertex.z(); // ->tree
          eleSmearE = par->smearE; // ->tree
          eleSmearP = par->smearP; // ->tree
          eleSmearPID = par->smearPID; // ->tree
          eleSmearVtx = par->smearVtx; // ->tree
          eleFound = true;
          eleEmax = eleE;
        }
      }
    } // eo electron loop
    if(!eleFound) {
      if(verbose>=1) print("[+] no scattered e-\n");
      return;
    }

    // electron momentum components
    eleP = vecEle.P(); // ->tree
    elePt = vecEle.Pt(); // ->tree
    eleEta = vecEle.Eta(); // ->tree
    elePhi = vecEle.Phi(); // ->tree


    //  // calculate DIS kinematics from scattered electron
    vecW = vecEbeam + vecPbeam - vecEle;
    vecQ = vecEbeam - vecEle;
    W = vecW.M(); // ->tree
    Q2 = -1*vecQ.M2(); // ->tree
    Nu = vecPbeam.Dot(vecQ) / PartMass(kP); // ->tree
    x = Q2 / ( 2 * vecQ.Dot(vecPbeam)); // ->tree
    y = vecPbeam.Dot(vecQ) / vecPbeam.Dot(vecEbeam); // ->tree
    // calculate boost vectors
    boostvecCom = vecQ + vecPbeam;
    ComBoost = -1*boostvecCom.BoostVector();



    //---------------------
    // dihadron loop
    //---------------------
    // pairing algorithm for dihadrons
    // - uses "observables" in Constants.h to define which hadrons to use
    // - if you want to add more particles, add them to the observables
    //   enumerators in Constants.h, and update Constants::OI and IO
    nh = 0;
    if(verbose>=2) { Tools::PrintSeparator(20,"."); print("dihadrons:\n"); };
    for(auto hadA : parList_full[s]) {
      if(!(hadA->is_stable) && !(hadA->pdg==111)) continue;
      for(auto hadB : parList_full[s]) {
        if(!(hadB->is_stable) && !(hadB->pdg==111)) continue;
        had[qA] = hadA;
        had[qB] = hadB;

        // get pdg
        for(h=0; h<2; h++) hadPDG[nh][h] = had[h]->pdg; // ->tree

        // convert PID to local particle index "Idx" (see Constants.h)
        for(h=0; h<2; h++) hadIdx[nh][h] = PIDtoIdx(had[h]->pdg); // ->tree

        // check if they are on the "observables" list, and ordered properly
        // so we don't double count
        if(ObservablePair(hadIdx[nh][qA],hadIdx[nh][qB])) {

          // check the particle "I": id in the list of this event's particles
          for(h=0; h<2; h++) hadI[nh][h] = had[h]->idx;

          // avoid double-counting
          if(hadI[nh][qA]==hadI[nh][qB]) continue;
          if(hadIdx[nh][qA]==hadIdx[nh][qB] && hadI[nh][qA]>hadI[nh][qB]) continue;

          // set pairType
          pairType[nh] = EncodePairType(hadIdx[nh][qA],hadIdx[nh][qB]); // ->tree

          // set matching booleans
          if (had[qA]->smearMatch == 1 && had[qB]->smearMatch == 1) pairMatch[nh] = 1;
          else pairMatch[nh] = 0;


          // set hadron momenta and vertices
          for(h=0; h<2; h++) {
            vecHad[h].SetXYZM(had[h]->p.px(),had[h]->p.py(),had[h]->p.pz(),had[h]->p.M());
            hadM[nh][h] = vecHad[h].M(); // ->tree
            if(s>0 && had[h]->pdg == 111) rootOutput.ReconPionM->Fill(vecHad[h].M());
            hadE[nh][h] = vecHad[h].E(); // ->tree
            hadP[nh][h] = vecHad[h].P(); // ->tree
            hadPt[nh][h] = vecHad[h].Pt(); // ->tree
            hadEta[nh][h] = vecHad[h].Eta(); // ->tree
            hadPhi[nh][h] = vecHad[h].Phi(); // ->tree

            hadDirX[nh][h] = vecHad[h].Px()/vecHad[h].E();
            hadDirY[nh][h] = vecHad[h].Py()/vecHad[h].E();
            hadDirZ[nh][h] = vecHad[h].Pz()/vecHad[h].E();

            hadVertex[nh][h][eX] = had[h]->vertex.x(); // ->tree
            hadVertex[nh][h][eY] = had[h]->vertex.y(); // ->tree
            hadVertex[nh][h][eZ] = had[h]->vertex.z(); // ->tree

            hadSmearE[nh][h] = had[h]->smearE; // ->tree
            hadSmearP[nh][h] = had[h]->smearP; // ->tree
            hadSmearPID[nh][h] = had[h]->smearPID; // ->tree
            hadSmearVtx[nh][h] = had[h]->smearVtx; // ->tree
          };


          // compute 4-momenta Ph and R
          vecPh = vecHad[qA] + vecHad[qB];
          vecR = 0.5 * ( vecHad[qA] - vecHad[qB] );

          // get 3-momenta from 4-momenta
          pQ = vecQ.Vect();
          pL = vecEle.Vect();
          pPh = vecPh.Vect();
          pR = vecR.Vect();
          for(h=0; h<2; h++) pHad[h] = vecHad[h].Vect();


          // compute z
          for(h=0; h<2; h++) {
            hadZ[nh][h] = vecPbeam.Dot(vecHad[h]) / vecPbeam.Dot(vecQ); // ->tree
          };
          z[nh] = vecPbeam.Dot(vecPh) / vecPbeam.Dot(vecQ); // ->tree

          //  compute invariant mass (and hadron masses)
          M[nh] = vecPh.M(); // ->tree

          // compute alpha
          alpha[nh] = Tools::AngleSubtend(pHad[qA],pHad[qB]); // ->tree

          // compute missing mass
          vecMx = vecW - vecPh;
          Mx[nh] = vecMx.M(); // ->tree

          // compute xF (in CoM frame)
          vecPh_com = vecPh; vecPh_com.Boost(ComBoost);
          vecQ_com = vecQ;   vecQ_com.Boost(ComBoost);
          pPh_com = vecPh_com.Vect();
          pQ_com = vecQ_com.Vect();
          for(h=0; h<2; h++) {
            vecHad_com[h] = vecHad[h]; vecHad_com[h].Boost(ComBoost);
            pHad_com[h] = vecHad_com[h].Vect();
          };
          xF[nh] = 2 * pPh_com.Dot(pQ_com) / (W * pQ_com.Mag()); // ->tree
          for(h=0; h<2; h++)
            hadXF[nh][h] = 2 * pHad_com[h].Dot(pQ_com) / (W * pQ_com.Mag()); // ->tree

          // compute theta
          zeta[nh] = 2 * vecR.Dot(vecPbeam) / ( vecPh.Dot(vecPbeam) ); // ->tree
          for(h=0; h<2; h++) MRterm[nh][h] = TMath::Sqrt( vecHad[h].M2() + pR.Mag2() );
          theta[nh] = TMath::ACos(  ( MRterm[nh][qA] - MRterm[nh][qB] - M[nh]*zeta[nh] ) / ( 2*pR.Mag() )); // ->tree


          // compute azimuthal angles phiH, phiR, phiS
          // -- boost to proton rest frame
          boostvecProt = vecPbeam;
          protBoost = -1*boostvecProt.BoostVector();
          vecPh_prot = vecPh; vecPh_prot.Boost(protBoost);
          vecQ_prot = vecQ;   vecQ_prot.Boost(protBoost);
          vecL_prot = vecEle; vecL_prot.Boost(protBoost);
          vecP_prot = vecPbeam; vecP_prot.Boost(protBoost);
          pPh_prot = vecPh_prot.Vect();
          pQ_prot = vecQ_prot.Vect();
          pL_prot = vecL_prot.Vect();
          for(h=0; h<2; h++) {
            vecHad_prot[h] = vecHad[h]; vecHad_prot[h].Boost(protBoost);
            pHad_prot[h] = vecHad_prot[h].Vect();
          };
          /*
          printf("-----------\n");
          printf("vecP_prot\n"); vecP_prot.Print(); // check the boost
          printf("vecPh_prot\n"); vecPh_prot.Print();
          printf("vecQ_prot\n");   vecQ_prot.Print();
          printf("vecL_prot\n"); vecL_prot.Print();
          for(h=0; h<2; h++){printf("vecHad_prot\n"); vecHad_prot[h].Print();};
          */

          // -- compute PhiH
          phiH[nh] = Tools::PlaneAngle(pQ_prot, pL_prot, pQ_prot, pPh_prot); // ->tree
          //    for(h=0; h<2; h++) {
          //        hadphiH[h] = Tools::PlaneAngle(pQ_prot, pL_prot, pQ_prot, pHad_prot[h]); // ->tree
          //    };

          // --compute phiR
          // - TODO: double check this phiR definition is the preferred one
          for(h=0; h<2; h++) {
            pHad_prot_perp[h] = Tools::Reject(pHad_prot[h],pQ_prot);
          };
          pRT_prot = (1.0/z[nh]) * ( hadZ[nh][qB]*pHad_prot_perp[qA]  -  hadZ[nh][qA]*pHad_prot_perp[qB] );
          phiR[nh]= Tools::PlaneAngle(pQ_prot, pL_prot, pQ_prot, pRT_prot); // ->tree

          // -- compute phiS
          spinBivec.SetXYZ(0,spinP,0);
          //spinBivec = Tools::Reject(spinBivec,pQ); // pedantic (no effect)
          phiS[nh] = Tools::PlaneAngle(pQ_prot, pL_prot, pQ_prot, spinBivec); // ->tree

          // calculate other Ph, R, RT, and RPerp tree branches
          pPhPerp = Tools::Reject(pPh_prot,pQ_prot); // proton rest frame
          PhPerpMag[nh] = pPhPerp.Mag(); // ->tree
          for(h=0; h<2; h++) {
            pHadPerp[h] = Tools::Reject(pHad_prot[h],pQ_prot); // proton rest frame
            hadPperp[nh][h] = pHadPerp[h].Mag(); // ->tree
          };
          /*
          PhMag[nh] = pPh.Mag(); // ->tree
          RMag[nh] = pR.Mag(); // ->tree
          RTMag[nh] = pRT_prot.Mag(); // ->tree
          if(PhMag[nh]>0) {
            PhEta[nh] = pPh.Eta(); // ->tree
            PhPhi[nh] = pPh.Phi(); // ->tree
          } else {
            PhEta[nh] = 0;
            PhPhi[nh] = 0;
          };
          */


          // set extra branches for CLAS asymmetry code compatibility
          /*
          runnum[nh] = 0;
          eleStatus[nh] = -2231; // typical CLAS trigger electron
          eleChi2pid[nh] = 0;
          for(int k=0; k<3; k++) {
            eleFidPCAL[nh][k] = true;
            eleFidDC[nh][k] = true;
          };
          for(h=0; h<2; h++) {
            hadStatus[nh][h] = 2000; // CLAS FD
            hadChi2pid[nh][h] = 0;
          };
          */


          // get asymmetry weights
          if (s<1){
            double mean, unc;
            autTesting->getWeight(Q2, M[nh], z[nh], x, mean, unc);
            myAUTweight[nh] = 1 + spinP * mean * TMath::Sin(phiR[nh] + phiS[nh]);;
            weightLowerLimit[nh] = myAUTweight[nh] - unc;
            weightUpperLimit[nh] = myAUTweight[nh] + unc;
          }


          if(verbose>=2) {
            print("...\n");
            for(h=0; h<2; h++) this->PrintParticle(had[h]);
          };

          // increment dihadron counter
          nh++;

        } // observable pair
      } // eo hadB loop
    } // eo hadA loop

    // fill trees
    numHadronPairs = nh;
    if(s>0) rootOutput.tree_s->Fill();
    else rootOutput.tree_ns->Fill();

  } // smear/unsmear vector
}


void DihadronProcessor::Finish() {

  Tools::PrintTitleBox("DihadronProcessor::Finish()");

  TTree *Tns = rootOutput.tree_ns;

  int true_numHadronPairs;
  float true_weight[true_numHadronPairs];
  float true_upperLim[true_numHadronPairs];
  float true_lowerLim[true_numHadronPairs];
  int true_pairMatch[true_numHadronPairs];
  int true_hadI[true_numHadronPairs][2];
  std::vector <std::vector < std::tuple<int, int, float, float, float> > > true_hads;

  Tns->SetBranchAddress("numHadronPairs",&true_numHadronPairs);
  Tns->SetBranchAddress("pairMatch",&true_pairMatch);
  Tns->SetBranchAddress("weight",&true_weight);
  Tns->SetBranchAddress("weightUpperLimit",&true_upperLim);
  Tns->SetBranchAddress("weightLowerLimit",&true_lowerLim);
  Tns->SetBranchAddress("hadI",&true_hadI);

  for(int i = 0; i < Tns->GetEntries(); i++){
    Tns->GetEntry(i);
    std::vector <std::tuple <int, int, float, float, float> > temp_hads;
    for (int n=0; n<true_numHadronPairs; n++){
      if (true_pairMatch[n] == 1) {
        std::tuple <int, int, float, float, float> true_pair;
        true_pair = make_tuple(true_hadI[n][0], true_hadI[n][1], true_weight[n], true_upperLim[n], true_lowerLim[n]);
        temp_hads.push_back(true_pair);
      }
    }
    true_hads.push_back(temp_hads);
  }

  TTree *Ts = rootOutput.tree_s;

  int smear_numHadronPairs;
  float smear_weight[smear_numHadronPairs];
  float smear_upperLim[smear_numHadronPairs];
  float smear_lowerLim[smear_numHadronPairs];
  int smear_pairMatch[smear_numHadronPairs];
  int smear_hadI[smear_numHadronPairs][2];
  std::vector <std::vector < std::tuple<int, int, int> > > smear_hads;

  Ts->SetBranchAddress("numHadronPairs",&smear_numHadronPairs);
  Ts->SetBranchAddress("pairMatch",&smear_pairMatch);
  Ts->SetBranchAddress("hadI",&smear_hadI);


  for(int i = 0; i < Ts->GetEntries(); i++){
    Ts->GetEntry(i);
    std::vector <std::tuple <int, int, int> > temp_hads;
    for (int n=0; n<smear_numHadronPairs; n++){
      if (smear_pairMatch[n] == 1) {
        std::tuple <int, int, int> smear_pair;
        smear_pair = make_tuple(smear_hadI[n][0], smear_hadI[n][1], n);
        temp_hads.push_back(smear_pair);
      }
    }
    smear_hads.push_back(temp_hads);
  }

  TBranch *weight_branch = Ts->Branch("weight", smear_weight, "smear_weight[numHadronPairs]/F");
  TBranch *upperweight_branch = Ts->Branch("weightUpperLimit", smear_upperLim, "smear_upperLim[numHadronPairs]/F");
  TBranch *lowerweight_branch = Ts->Branch("weightLowerLimit", smear_lowerLim, "smear_lowerLim[numHadronPairs]/F");

  for(int i = 0; i < Ts->GetEntries(); i++){
    Ts->GetEntry(i);
    for(int j = 0; j < smear_numHadronPairs; j++){
      smear_weight[j] = 1;
      smear_upperLim[j] = 1;
      smear_lowerLim[j] = 1;
    }
    for (auto smear_pair : smear_hads[i]){
      for (auto true_pair : true_hads[i]){
        if (get<0>(smear_pair) == get<0>(true_pair) && get<1>(smear_pair) == get<1>(true_pair)){
          smear_weight[get<2>(smear_pair)] = get<2>(true_pair);
          smear_upperLim[get<2>(smear_pair)] = get<3>(true_pair);
          smear_lowerLim[get<2>(smear_pair)] = get<4>(true_pair);
        }
      }
    }
    weight_branch->Fill();
    upperweight_branch->Fill();
    lowerweight_branch->Fill();
  }

  if(errThrown) {
    print("ERROR: DihadronProcessor threw error(s) (see above)\n");
  } else {
    /*
       rootOutput.tree->SetScanField(0);
    //rootOutput.tree->Scan("evtNr:eleE:hadE[0]:hadE[1]:Z[0]:Z[1]");
    //rootOutput.tree->Scan("evtNr:x-x_pythia:y-y_pythia");
    //rootOutput.tree->Scan("evtNr:Q2-Q2_pythia:W-W_pythia:Nu-Nu_pythia");
    rootOutput.tree->Scan("evtNr:pairType:hadIdx[0]:hadIdx[1]:hadE[0]:hadE[1]");
    */
  };

};



// print event record info
void DihadronProcessor::PrintEvent() {
  Tools::PrintSeparator(70,"=");
  print("evtNr={}\n",
      evtNr
      );
  Tools::PrintSeparator(70,"-");
};


// print particle record info
void DihadronProcessor::PrintParticle(const DihadronInputParticle * inPar) {
  print("idx={} pid={} is_stable={} p=({}, {}, {})\n",
      inPar->idx,
      inPar->pdg,
      inPar->is_stable,
      inPar->p.px(), inPar->p.py(), inPar->p.pz()
      );
};
