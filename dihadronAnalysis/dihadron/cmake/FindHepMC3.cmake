# - Locate HepMC3 library
# in a directory defined via  HEPMC3_ROOT_DIR or HEPMC3_DIR environment variable
# Defines:
#
#  HEPMC3_FOUND
#  HEPMC3_INCLUDE_DIR
#  HEPMC3_INCLUDE_DIRS (not cached)
#  HEPMC3_LIBRARIES
#  HEPMC3_FIO_LIBRARIES

find_path(HEPMC3_INCLUDE_DIR HepMC3/GenEvent.h
        HINTS
        $ENV{HEPMC3_DIR}/include
        $ENV{HEPMC3_ROOT_DIR}/include
        ${HEPMC3_DIRECTORY}/include
        ${HEPMC3_DIR}/include)

find_library(HEPMC3_LIBRARY
        NAMES HepMC3
        HINTS
        $ENV{HEPMC3_ROOT_DIR}
        $ENV{HEPMC3_DIR}
        ${HEPMC3_DIRECTORY}
        ${HEPMC3_DIR}
        PATH_SUFFIXES lib lib64
        )

get_filename_component(HEPMC3_LIBRARY_DIR ${HEPMC3_LIBRARY} PATH)
#set(HEPMC3_FIO_LIBRARIES "-L${HEPMC3_LIBRARY_DIR} -lHepMC3fio")


# handle the QUIETLY and REQUIRED arguments and set HEPMC3_FOUND to TRUE if
# all listed variables are TRUE
#INCLUDE(FindPackageHandleStandardArgs)
find_package_handle_standard_args(HepMC3 DEFAULT_MSG HEPMC3_LIBRARY HEPMC3_INCLUDE_DIR)

mark_as_advanced(HepMC3_FOUND HEPMC3_INCLUDE_DIR HEPMC3_LIBRARY)

set(HEPMC3_INCLUDE_DIRS ${HEPMC3_INCLUDE_DIR})
set(HEPMC3_LIBRARIES ${HEPMC3_LIBRARY} )

message(STATUS "HEPMC3_INCLUDE_DIRS ${HEPMC3_INCLUDE_DIRS}")
message(STATUS "HEPMC3_LIBRARIES ${HEPMC3_LIBRARIES}")
