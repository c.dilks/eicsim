#include <JANA/JEventProcessor.h>
#include <JANA/JFactoryGenerator.h>

#include "DihadronProcessor.h"

/// This class is temprorary here while JANA2 API is getting to perfection.
/// Here we just list JFactories defined in this plugin
struct DihadronInputParticleGenerator:public JFactoryGenerator {
  void GenerateFactories(JFactorySet *factory_set) override {
    factory_set->Add( new JFactoryT<DihadronInputParticle>());
  }
};

extern "C"
{
  void InitPlugin(JApplication *app)
  {

    // This code is executed when the plugin is attached.
    // It should always call InitJANAPlugin(app) first, and then do one or more of:
    //   - Read configuration parameters
    //   - Register JFactoryGenerators
    //   - Register JEventProcessors
    //   - Register JEventSourceGenerators (or JEventSources directly)
    //   - Register JServices

    InitJANAPlugin(app);

    app->Add(new DihadronProcessor(app));
    app->Add(new DihadronInputParticleGenerator());
  }
}
