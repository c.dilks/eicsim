#ifndef DIHADRON_ROOT_OUTPUT_HEADER
#define DIHADRON_ROOT_OUTPUT_HEADER

#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>
#include "Tools.h"
#include "Constants.h"

using namespace dih;

class DihadronRootOutput
{
  public:

    //////////////////////////////////////////////////////
    void init(TFile *file)
    {
      // mutex lock prevents TFile streaming interference between threads
      std::lock_guard<std::recursive_mutex> locker(lock);

      file->cd();

      tree_ns = new TTree("tree_ns", "dihadron tree_ns");
      tree_s = new TTree("tree_s", "dihadron tree_s");
      ReconPionM = new TH1D("Reconstructed Pi0 Mass","Reconstructed Pi0 Mass;M",200, 0, 4);

    } // eo init
    //////////////////////////////////////////////////////


    std::recursive_mutex lock;

    TTree * tree_ns; // dihadron tree
    TTree * tree_s; // dihadron tree
    TH1D * ReconPionM;

  private:
};

#endif
