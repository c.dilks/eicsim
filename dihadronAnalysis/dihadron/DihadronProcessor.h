#ifndef DIHADRON_PROCESSOR_HEADER
#define DIHADRON_PROCESSOR_HEADER

#include <thread>
#include <mutex>

#include <JANA/JEventProcessor.h>
#include <JANA/JObject.h>
#include <JANA/JEvent.h>
#include <ejana/EServicePool.h>

#include <Math/Point3D.h>
#include <Math/Vector4D.h>
#include <TRandom.h>
#include <TRandomGen.h>
#include <TLorentzVector.h>
#include <TVector3.h>

#include <fmt/core.h>

#include <MinimalistModel/McGeneratedParticle.h>
#include <ejana/plugins/io/lund_reader/LundEventData.h>
#include <ejana/plugins/io/hepmc_reader/HepMCEventData.h>

#include "DihadronRootOutput.h"
#include "Tools.h"
#include "Constants.h"
#include "TRandom.h"

#include "AUTweight.h"

class JApplication;

class DihadronInputParticle:public JObject {
  public:
    ROOT::Math::PxPyPzEVector E;
    ROOT::Math::PxPyPzMVector p;
    ROOT::Math::XYZPoint vertex;
    int idx;
    bool is_stable;
    int pdg;
    int charge;
    int photA_idx;
    int photB_idx;
    int mother_second_idx;
    int mother_idx;
    int pionMatch;
    bool pion_mother;
    int mother_pdg;
    int mother_second_pdg;
    int grand_mother_id;
    int grand_mother_pdg;
    bool smearMatch;
    bool smearE;
    bool smearP;
    bool smearPID;
    bool smearVtx;
};


class DihadronProcessor : public JEventProcessor
{
  public:

    // Constructor just applies
    explicit DihadronProcessor(JApplication *app=nullptr):
      JEventProcessor(app),
      services(app)
    {
      SetTypeName("DihadronProcessor");
    };

    // This is called once before the first call to the Process method
    // below. You may, for example, want to open an output file here.
    // Only one thread will call this.
    void Init() override;

    //----------------------------
    // Process
    //
    // This is called for every event. Multiple threads may call this
    // simultaneously. If you write something to an output file here
    // then make sure to protect it with a mutex or similar mechanism.
    // Minimize what is done while locked since that directly affects
    // the multi-threaded performance.
    void Process(const std::shared_ptr<const JEvent>& event) override;
    
    //----------------------------
    // Finish
    //
    // This is called once after all events have been processed. You may,
    // for example, want to close an output file here.
    // Only one thread will call this.
    void Finish() override;

    // additional methods
    void PrintEvent();
    void PrintParticle(const DihadronInputParticle * inPar);

  private:
    DihadronRootOutput rootOutput;
    ej::EServicePool services;
    int verbose; // verbose output level

    TString fileType;

    /////////////////////////////
    // BEAM PARAMETERS
    Float_t EbeamEn = 5; // range [2.5,18]
    Float_t PbeamEn = 41; // range [41,275]
    Float_t crossingAngle = 22e-3; // [rad] (BEAST crossing angle is 22mrad)
    /////////////////////////////
    
    Float_t EbeamP,PbeamP;
    TLorentzVector vecPbeam, vecEbeam;

    // electron
    TLorentzVector vecEle, vecEleTmp, vecQ, vecW, vecMx;
    Double_t eleETmp;
    Double_t eleEmax;
    Bool_t eleFound;

    //photons
    TLorentzVector vecPhot[2], vecPhot_tmp[2];
    Int_t smearMatch[2];

    //hadrons
    int h;
    TLorentzVector vecHad[2];
    TLorentzVector vecPh, vecR;
    TVector3 pQ,pL,pPh,pR,spinBivec;
    TVector3 pHad[2];
    TVector3 pHad_prot_perp[2];
    TVector3 pRT_prot,pRPerp,pPhPerp;
    TVector3 pHadPerp[2];

    TLorentzVector boostvecCom,boostvecProt;
    TVector3 ComBoost,protBoost;
    TLorentzVector vecPh_com, vecQ_com;
    TLorentzVector vecPh_prot,vecQ_prot, vecL_prot, vecP_prot;
    TLorentzVector vecHad_com[2];
    TLorentzVector vecHad_prot[2];
    TVector3 pPh_com, pQ_com;
    TVector3 pPh_prot,pQ_prot, pL_prot;
    TVector3 pHad_com[2];
    TVector3 pHad_prot[2];


    // KINEMATIC VARS ///////////////////////////////////////
    static const Int_t nhMax = 1000;
    Int_t pairMatch[nhMax];
    Int_t numHadronPairs;

    Float_t x, y, Q2, W, Nu;
    Float_t x_pythia, Q2_pythia, y_pythia, W_pythia, Nu_pythia;
    Int_t polarization;

    // electron
    Float_t eleE,eleP,elePt,eleEta,elePhi;
    Float_t eleVertex[3];
    Int_t eleI;
    Bool_t eleSmearE,eleSmearP,eleSmearPID,eleSmearVtx;

    // hadrons
    Int_t pairType[nhMax];
    Int_t hadPDG[nhMax][2];
    Int_t hadIdx[nhMax][2];
    Float_t hadM[nhMax][2];
    Float_t hadE[nhMax][2];
    Float_t hadP[nhMax][2];
    Float_t hadPt[nhMax][2]; // lab frame transverse momentum
    Float_t hadPperp[nhMax][2]; // momentum transverse to q (cf PhPerp)
    Float_t hadEta[nhMax][2];
    Float_t hadPhi[nhMax][2];
    Float_t hadPhiH[nhMax][2];
    Float_t hadVertex[nhMax][2][3];
    Float_t hadDirX[nhMax][2];
    Float_t hadDirY[nhMax][2];
    Float_t hadDirZ[nhMax][2];
    Bool_t hadSmearE[nhMax][2];
    Bool_t hadSmearP[nhMax][2];
    Bool_t hadSmearPID[nhMax][2];
    Bool_t hadSmearVtx[nhMax][2];

    // photons
    Int_t photIdx[2], photIdx_tmp[2];
    Int_t photMotherI[2], photMotherI_tmp[2];
    Int_t photMotherPdg[2], photMotherPdg_tmp[2];
    Int_t photI[2], photI_tmp[2];
    Float_t photE[2], photE_tmp[2];
    TLorentzVector vecHad_tmp;
    Float_t hadM_tmp;
    Float_t E_asym; // photon energy asymmetry
    Float_t photP[2];
    Float_t photPt[2];
    Float_t photEta[2];
    Float_t photPhi[2];
    Float_t photPhiH[2];
    Float_t photVertex[2][3];
    Bool_t photSmearE[2];
    Bool_t photSmearP[2];
    Bool_t photSmearPID[2];
    Bool_t photSmearVtx[2];

    //pair kinematics
    Int_t nh;

    Float_t PhMag[nhMax]; // dihadron total momentum
    Float_t PhPerpMag[nhMax]; // transverse component of dihadron total momentum (perp frame)
    Float_t PhEta[nhMax]; // pseudorapidity of dihadron pair
    Float_t PhPhi[nhMax]; // azimuth of dihadron pair
    Float_t RMag[nhMax]; // dihadron relative momentum
    Float_t RTMag[nhMax]; // transverse componet R (T-frame, and proton rest frame)
    Float_t RPerpMag[nhMax]; // transverse componet of relative momentum (perp-frame)

    Float_t phiH[nhMax]; // angle[ reaction_plane, Ph^q ]
    Float_t phiR[nhMax]; // angle[ reaction_plane, RT^q ]
    Float_t phiS[nhMax]; // angle[ reaction_plane, S^q ]
    Float_t hadZ[nhMax][2]; /* fraction of energy of fragmenting parton
                        carried by the hadron */
    Float_t z[nhMax]; /* fraction of energy of fragmenting parton
                        carried by the dihadron */
    Float_t M[nhMax]; // dihadron invariant mass
    Float_t Mx[nhMax]; // missing mass
    Float_t xF[nhMax]; // feynman-x
    Float_t alpha[nhMax]; //opening angle between hadrons
    Float_t hadXF[nhMax][2]; // feynman-x for each hadron

    Float_t zeta[nhMax]; // lab-frame energy sharing
    Float_t theta[nhMax]; // CoM-frame angle between Ph and P1
    Float_t MRterm[nhMax][2]; // (used for computing theta)

    Int_t evtNr, evtNr_local;
    Int_t spinE,spinP;
    Int_t particleCnt;

    // extra variables for compatibility with CLAS dihadron asymmetry code
    Int_t runnum[nhMax];
    Int_t eleStatus[nhMax];
    Float_t eleChi2pid[nhMax];
    Bool_t eleFidPCAL[nhMax][3];
    Bool_t eleFidDC[nhMax][3];
    Int_t hadI[nhMax][2];
    Int_t hadIA[nhMax];
    Int_t hadIB[nhMax];
    Int_t hadStatus[nhMax][2];
    Float_t hadChi2pid[nhMax][2];

    Int_t electronSign; // sign of z-component of electron

    TRandom * RNG;
    bool errThrown;
    Int_t i1,i2;

    const DihadronInputParticle * had[2];
    const DihadronInputParticle * phot[2];

    AUTweight * autTesting;
    Float_t myAUTweight[nhMax];
    Float_t weightLowerLimit[nhMax];
    Float_t weightUpperLimit[nhMax];
};

#endif
