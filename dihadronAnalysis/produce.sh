#!/bin/bash

function exe {
  python3 runEjana.py -t pythia -i dataPythia/pythia6.${1}x${2}.noRC.txt \
  -o dataRoot/pythia_${1}x${2}_smear.root \
  -n 0 -e $1 -p $2 -s handbook
  python3 runEjana.py -t pythia -i dataPythia/pythia6.${1}x${2}.noRC.txt \
  -o dataRoot/pythia_${1}x${2}_nosmear.root \
  -n 0 -e $1 -p $2
}

exe 5 41
#exe 5 100
#exe 10 100
exe 18 275
