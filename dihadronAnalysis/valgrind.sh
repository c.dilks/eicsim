#!/bin/bash

#************************************************
# note: to install valgrind on docker container: 
# `sudo unminimize; sudo apt install valgrind`
#************************************************

# link `dihadron.so` to pwd, since `ejana --so=dihadron/dihadron.so` fails
ln -s dihadron/dihadron.so

# run valgrind
valgrind \
  --log-file="mem.log" \
  --leak-check=full \
  --num-callers=50 \
  --verbose \
  --suppressions=$ROOTSYS/etc/valgrind-root.supp \
ejana \
  -Pplugins=lund_reader,event_writer,eic_smear,dihadron \
  -Pdihadron:filetype=pythia \
  -Pdihadron:EbeamEn=18 \
  -Pdihadron:PbeamEn=275 \
  -Peic_smear:detector=handbook \
  -Peic_smear:throw=0 \
  -Peic_smear:strict=0 \
  -Pnevents=1000 \
  -Pnthreads=1 \
  -Poutput=test.root \
  dataPythia/pythia6.18x275.noRC.txt

# cleanup
rm dihadron.so
