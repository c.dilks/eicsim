#!/bin/bash

# generate table of radiative corrections; table will be stored in `radgen/`,
# then generate events with radiative corrections from `radgen/`
#for en in 5x41 18x275; do
  #mkdir -p radgen
  #rm -r radgen
  #mkdir -p radgen
  #pythiaeRHIC < steer/official.pythia6.${en}.genRC.eic |\
    #tee dataPythia/pythia6.${en}.genRC.log
  #pythiaeRHIC < steer/official.pythia6.${en}.RC.eic |\
    #tee dataPythia/pythia6.${en}.RC.log
#done

# generate events without radiative corrections
for en in 5x41 18x275; do
  pythiaeRHIC < steer/official.pythia6.${en}.noRC.eic |\
    tee dataPythia/pythia6.${en}.noRC.log
done


# move event files
mv pythia6*.txt dataPythia/
