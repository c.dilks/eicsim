import sys, getopt, multiprocessing, re
from pyjano.jana import Jana, PluginFromSource

testPlugin = PluginFromSource('./minimal_example', name='minimal_example')

numCPU = multiprocessing.cpu_count()
jana = Jana(nevents=0, nthreads=numCPU, output='test.root')

jana.plugin('lund_reader')\
    .plugin('event_writer')\
    .plugin(testPlugin)\
    .plugin('eic_smear',detector='handbook',throw=1,strict=0)\
    .source('dataPythia/pythia6.18x275.noRC.txt')

jana.run()
