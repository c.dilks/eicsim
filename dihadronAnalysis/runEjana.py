import sys, getopt, multiprocessing, re
from pyjano.jana import Jana, PluginFromSource


def main(argv):

# ARGUMENTS
#####################

    enE = 10 # electron energy
    enP = 100 # proton energy
    numEvents = 20
    enableSmear = False
    smearDetector = ''
    verbose = 0

    helpStr = f'''
  {sys.argv[0]} -t <fileType> -i <inputFile> [ADDITIONAL ARGS]
    
    Required Arguments:

        input file type is set by `-t <fileType>`, which can be:
          pythia
          lund
          hepmc

        input file name is set by `-i <inputFile`
    
    Additional Arguments:

        -o <outputRootFile>  output ROOT file name (default=inputFile.root)
        -n <numEvents>  number of events to run; set to 0 for all (default=20)
        -e <electronEnergy>  electron beam energy (default=10)
        -p <protonEnergy>  proton beam energy (default=100)
        -s <detector>  enable eic_smear, using one of the following detectors:
               jleic
               beast
               ephenix
               zeus
               handbook
        -v <level> enable verbosity: 0=off, 1=low, 2=high; (default=0)
        '''

    if(len(sys.argv)<=1):
        print(helpStr)
        sys.exit(2)
    try:
        opts, args = getopt.getopt(argv,"hi:o:e:p:n:s:t:v:")
    except getopt.GetoptError:
        print('\n\nERROR: invalid argument\n',helpStr)
        sys.exit(2)

    fileTypeDefined = False
    inputFileDefined = False
    outputFileDefined = False

    for opt, arg in opts:
        if(opt=='-i'):
            inputFile = arg
            inputFileDefined = True
        if(opt=='-t'):
            fileType = arg
            fileTypeDefined = True
        if(opt=='-o'): 
            outputFile = arg
            outputFileDefined = True
        if(opt=='-e'): enE = arg
        if(opt=='-p'): enP = arg
        if(opt=='-s'):
            enableSmear = True
            smearDetector = arg
        if(opt=='-n'): numEvents = arg
        if(opt=='-v'): verbose = arg
        if(opt=='-h'):
            print(helpStr)
            sys.exit()

    if(not fileTypeDefined or not inputFileDefined):
        print('\n\nERROR: need fileType (-t) and inputFile (-i) arguments\n',helpStr)
        sys.exit(2)
    if(not outputFileDefined):
        outputFile = re.sub("^.*\/","dataRoot/",inputFile) + ".root"

#####################

# build dihadron plugin

    dihadronPlugin = PluginFromSource('./dihadron', name='dihadron')


# initialize jana and setup plugins

    numCPU = multiprocessing.cpu_count()
    jana = Jana(nevents=numEvents, nthreads=numCPU, output=outputFile)

    if(fileType=="hepmc"): jana.plugin('hepmc_reader')
    elif(fileType=="lund"): jana.plugin('lund_reader')
    elif(fileType=="pythia"): jana.plugin('lund_reader')
    else:
        print('\n\nERROR: bad fileType (-t)\n',helpStr)
        sys.exit(2)

    jana.plugin('event_writer')\
        .plugin(dihadronPlugin,\
            verbose=verbose, EbeamEn=enE, PbeamEn=enP, fileType=fileType)\
        .source(inputFile)

    if(enableSmear):
        jana.plugin('eic_smear',detector=smearDetector,throw=0,strict=0)


# process events
    jana.run()



if __name__ == "__main__":
   main(sys.argv[1:])
