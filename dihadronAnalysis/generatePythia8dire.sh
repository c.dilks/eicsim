#!/bin/bash
# generate events using pythia, with dire parton shower plugin
# - output is a hepmc file

# arguments
if [ $# -lt 4 ]; then
  echo "usage: $0 [numEvents] [enE] [enP] [cmndFile]"
  exit
fi
numEvents=$1
enE=$2
enP=$3
infile=$4

# set outfile name
outfile=dataHepMC/dire_${enE}x${enP}.hepmc

# update ejpm env
source <(ejpm env)

# run dire+pythia8
dire --nevents $numEvents \
     --nthreads $(nproc --all) \
     --input $infile \
     --setting Beams:eA=${enP} \
     --setting Beams:eB=${enE} \
     --hepmc_output $outfile

# cleanup
rm -v *shower*weights.dat
echo "PRODUCED HEPMC FILE $outfile"
